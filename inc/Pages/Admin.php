<?php 

/**
 * 
 * @package expo_notification
 * 
 */

 
namespace Inc\Pages;

use \Inc\Base\BaseController;


class Admin extends BaseController
{
	public function register() {
		add_action( 'admin_menu', array( $this, 'add_admin_page' ) );
		add_action( 'admin_menu', array( $this, 'add_admin_sub_page' ) );
		add_action( 'admin_init', array( $this, 'registerFields' ) );
	}

	public function add_admin_page() 
	{
		add_menu_page( 'Expo notification Plugin', 
		'Expo notification', 
		'manage_options', 
		'expo_notification', 
		function () { require_once $this->plugin_path . 'templates/admin.php'; }, 
		'dashicons-share-alt2', 110 );
	}

	public function add_admin_sub_page(){


		add_submenu_page("expo_notification",
		'Expo notification Devices', 
		'Devices', 
		'manage_options', 
		'expo_notification_devices', 
		function () { require_once $this->plugin_path . 'templates/devices.php'; } );

		add_submenu_page("expo_notification",
		'Expo notification settings', 
		'Settings', 
		'manage_options', 
		'expo_notification_settings', 
		function () { require_once $this->plugin_path . 'templates/settings.php'; } );
	}

	public function setFields()
	{		
		add_settings_field(
			'key_expo_notif_device',
			'Key expo notification',
			array($this,"input"),
			'expo_notification_settings',
			'expo_notification_admin_index',
			array('label_for' => 'key_expo_notif_device','class' => 'key_expo_notif')
		);
	}

	public function input()
	{
		$input =  '<input type="text" 
					class="regular-text"
					name="key_expo_notif_device" 
					value="'.esc_attr( get_option( 'key_expo_notif_device' )).'" 
					placeholder="your key">';

		echo $input;			
	}

	public function setSections()
	{
		add_settings_section(
			'expo_notification_admin_index',
			'Settings',
			function() {echo '';},
			'expo_notification_settings'
		);
	}

	public function registerFields()
	{
		register_setting( 
			'expo_notification_options_group',
			'key_expo_notif_device',
			function($input){ return $input;}
		);
		$this->setSections();
		$this->setFields();
	}
}