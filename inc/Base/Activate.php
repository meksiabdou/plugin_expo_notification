<?php
/**
 * @package  expo_notification
 */
namespace Inc\Base;

class Activate
{
	public static function activate() 
	{    
		global $wpdb;
	
		$tb = $wpdb->prefix."expo_notif_device";
	
		$sql = "CREATE TABLE IF NOT EXISTS $tb (
				id INTEGER NOT NULL AUTO_INCREMENT,
				token varchar(200) UNIQUE,
				platform varchar(100),
				date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY (id)
			)";
	
			$wpdb->query($sql);

			$key = "aelhd".rand(100,99999)."584hs";
    
			$wpdb->query("INSERT IGNORE INTO ".$wpdb->prefix."options (option_name, option_value) 
						VALUES ('key_expo_notif_device','$key')");
			
		flush_rewrite_rules();
	}
}