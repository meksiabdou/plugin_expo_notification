<?php
/**
 * @package  expo_notification
 */
namespace Inc\Base;

class SettingsLinks
{
	public function register() 
	{
		add_filter( "plugin_action_links_".plugin_basename(dirname(__FILE__,2)), array( $this, 'settings_link' ) );
	}

	public function settings_link( $links ) 
	{
		$links[] = '<a href="admin.php?page=expo_notification_settings">Settings</a>';
		return $links;
	}
}