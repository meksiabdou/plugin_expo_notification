<?php 

/**
 * 
 * @package expo_notification
 * 
 */

 
namespace Inc\Api;

class Api 
{
	public function register() {

        add_action( 'rest_api_init', array($this,'send_notificarion'));

        add_action( 'rest_api_init', function () {

            register_rest_route( 'expo_notification/v2', '/add_device', array(
                'methods' => 'POST',
                'callback' => array($this,'add_device'),
            ) );
        });
	}

    public function send_notificarion()
    {
        register_rest_route( 'expo_notification/v2', '/send', array(
            'methods' => 'POST',
            'callback' => array($this,'getUsers'),
        ) );
    }

    private function cleanData($string)
    {
        $string = strip_tags(urldecode($string)); //a-z 0-9~%.:_\-=()
        $string = str_replace("'","’",$string);
        $string = str_replace('"',"’",$string);
        $string = str_replace('/',"-",$string);
        $garbagearray = array('>','<','#','$','^',';',',','!','{','}','!','&','*',"'",'"','~','`','\ ','&nbsp;');
        $garbagecount = count($garbagearray);
        for ($i=0; $i<$garbagecount; $i++)
        {
            $string = str_replace(trim($garbagearray[$i]),'',$string);
        }
        return trim(strip_tags($string));
    }
    

    public function getUsers($data)
    {
        $id = $this->cleanData($data["id"]);
        $title = $this->cleanData(base64_decode($data["title"]));
        $key = $this->cleanData($data["key"]);
        $_key = esc_attr( get_option( 'key_expo_notif_device' ));

        $_result = "";

        if(empty($id) || empty($title) || empty($key) || empty($_key)) 
        {
            return array("status" => "Error");
        }
        elseif ($key !== $_key) 
        {
            return array("status" => "Error Key");
        }else
        {
            global $wpdb;
            $tb = $wpdb->prefix."expo_notif_device";
            $query = "
                    SELECT * FROM $tb
                    ORDER BY id DESC
            ";
            $query = $wpdb->get_results($query, OBJECT);

            if($query != null) {

                foreach($query as $divice):

                    $data_json[] = array(
                        "to" => $divice->token,
                        "title" => "مكرة نيوز",
                        "body" => $title,
                        "data" => ["id" => $id]
                    );  
                endforeach; 

                $result =  json_decode($this->Curl($data_json));
                //return $result;
                return array("status" => "Notification pushed");
            }
            else {
                return array("status" => "ok", "divice" => "0");
            }
        }

    }

    public function add_device($data)
    {
        $token = $this->cleanData($data["token"]);
        $platform = $this->cleanData($data["platform"]);
        $key = $this->cleanData($data["key"]);

        $_key = esc_attr( get_option( 'key_expo_notif_device' ));
        

        if(empty($token) || empty($platform) || empty($key) || empty($_key)  || $key != $_key) 
        {
            return array("status" => "Error");

        }else{
            global $wpdb;
        	
            $tb = $wpdb->prefix."expo_notif_device";
    
            $wpdb->query("INSERT IGNORE INTO $tb (token, platform) VALUES ('$token','$platform')");
        }

    }

    private function Curl($query)
    {
        $postdata = json_encode($query);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://exp.host/--/api/v2/push/send');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
}