<?php

/**
 * Trigger this file on Plugin uninstall
 *
 * @package  expo_notification
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	die;
}
