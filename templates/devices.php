<?php 
$post_page = 1; 
$items_per_page = 20;

if(!empty($_GET['post_page'])) 
{
    $post_page = filter_input(INPUT_GET, 'post_page', FILTER_VALIDATE_INT);
    if($post_page === false) 
    {
        $post_page = 1;
    }
}

$offset = ($post_page - 1) * $items_per_page;

    global $wpdb; 

    $tb = $wpdb->prefix."expo_notif_device";

    $query = "
                SELECT * FROM $tb
                ORDER BY id DESC
                LIMIT $offset,$items_per_page 
            ";
    $allQuery = "
                SELECT * FROM $tb
            ";      
 
  $postes = $wpdb->get_results($query, OBJECT);
  $allPostes = $wpdb->get_results($allQuery, OBJECT);

  $pages = ceil(count($allPostes) / $items_per_page);
?>


<div class="wrap">
    
    <h2>Devices</h2>

    <table class="wp-list-table widefat fixed striped posts">
        <thead>
                <tr>
                        <th>Token</th>
                        <th>Platform | Device name | Year class</th>
                        <th>Date</th>
                </tr> 
        </thead>

        <tbody id="the-list">
            <?php  foreach($postes as $post): ?>
                <tr class="">
                    <td>
                        <strong><?= $post->token ?></strong>
                    </td>	
                    <td>
                        <?= $post->platform ?>
                    </td>	
                    <td>
                        <?= $post->date ?>
                    </td>	
                </tr>
            <?php endforeach; ?>    
        </tbody>

        <tfoot>
                <tr>
                        <th>Token</th>
                        <th>Platform | Device name | Year class</th>
                        <th>Date</th>
                </tr> 
        </tfoot>
    </table>
    <div class="tablenav bottom" style="text-align: center;">
            <div class="tablenav-pages" style="float:none;">
                    <span class="pagination-links">
                        <a class="next-page" href="admin.php?page=expo_notification_devices&post_page=1" >
                                <span class="screen-reader-text"> </span>
                                <span aria-hidden="true">«</span>
                        </a>
                        <a class="prev-page" href="./admin.php?page=expo_notification_devices&post_page=<?= ($post_page - 1 == 0)? 1 : $post_page - 1 ?>" >
                            <span class="screen-reader-text"></span>    
                            <span  aria-hidden="true">‹</span>
                        </a>

                        <a class="next-page" href="admin.php?page=expo_notification_devices&post_page=<?= $post_page + 1 ?>" >
                                <span class="screen-reader-text"> </span>
                                <span aria-hidden="true">›</span>
                        </a>
                        <a class="next-page" href="admin.php?page=expo_notification_devices&post_page=<?= $pages ?>" >
                                <span class="screen-reader-text"> </span>
                                <span aria-hidden="true">»</span>
                        </a>
                    </span>
            </div>
    </div>    
</div>
