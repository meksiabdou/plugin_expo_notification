<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<?php 
$post_page = 1; 
$items_per_page = 20;

if(!empty($_GET['post_page'])) 
{
    $post_page = filter_input(INPUT_GET, 'post_page', FILTER_VALIDATE_INT);
    if($post_page === false) 
    {
        $post_page = 1;
    }
}

$offset = ($post_page - 1) * $items_per_page;

    global $wpdb; 
    $query = "
                SELECT * FROM $wpdb->posts
                WHERE post_status = 'publish' 
                AND post_type = 'post'
                AND post_date < NOW()
                ORDER BY post_date DESC
                LIMIT $offset,$items_per_page 
            ";
    $allQuery = "
                SELECT * FROM $wpdb->posts
                WHERE post_status = 'publish' 
                AND post_type = 'post'
                AND post_date < NOW()
            ";      
 
  $postes = $wpdb->get_results($query, OBJECT);
  $allPostes = $wpdb->get_results($allQuery, OBJECT);

  $pages = ceil(count($allPostes) / $items_per_page);
?>


<div class="wrap">
    
    <h2>Posts</h2>

    <table class="wp-list-table widefat fixed striped posts">
        <thead>
                <tr>
                        <th>
                                <a href="#">
                                    <span>Title</span>
                                </a>
                        </th>
                           <th>
                               Date
                            </th>
                        <th >
                            Action
                        </th>
                </tr> 
        </thead>

        <tbody id="the-list">
            <?php  foreach($postes as $post): ?>
                <tr class="">
                    <td>
                        <strong>
                            <a class="row-title" href="" aria-label="" ><?= $post->post_title ?></a>
                        </strong>
                    </td>
                    <td >
                        <?= $post->post_date ?>
                    </td>
                    <td >
                                <button title="Push Notification" class="button" style="line-height: 0;" onclick="expo_notification(<?= $post->ID  ?>,'<?= base64_encode($post->post_title) ?>')" >
									<span class="dashicons-before dashicons-external"></span>
								</button>
								<img src="<?= plugin_dir_url(dirname(__FILE__,1)) ?>assets/load.gif" id="load_<?= $post->ID ?>" style="height:22px;display:none;" />
                    </td>		
                </tr>
            <?php endforeach; ?>    
        </tbody>

        <tfoot>
            <tr>
                    <tr>
                            <th>
                                    <a href="#">
                                        <span>Title</span>
                                    </a>
                            </th>
                            <th>
                                Date
                                </th>
                            <th >
                                Action
                            </th>
                    </tr> 
            </tr>  
        </tfoot>
    </table>
    <div class="tablenav bottom" style="text-align: center;">
            <div class="tablenav-pages" style="float:none;">
                    <span class="pagination-links">
                        <a class="next-page" href="admin.php?page=expo_notification&post_page=1" >
                                <span class="screen-reader-text"> </span>
                                <span aria-hidden="true">«</span>
                        </a>
                        <a class="prev-page" href="./admin.php?page=expo_notification&post_page=<?= ($post_page - 1 == 0)? 1 : $post_page - 1 ?>" >
                            <span class="screen-reader-text"></span>    
                            <span  aria-hidden="true">‹</span>
                        </a>

                        <a class="next-page" href="admin.php?page=expo_notification&post_page=<?= $post_page + 1 ?>" >
                                <span class="screen-reader-text"> </span>
                                <span aria-hidden="true">›</span>
                        </a>
                        <a class="next-page" href="admin.php?page=expo_notification&post_page=<?= $pages ?>" >
                                <span class="screen-reader-text"> </span>
                                <span aria-hidden="true">»</span>
                        </a>
                    </span>
            </div>
    </div>    
</div>


<script>

function expo_notification(id = "",title) {

    $.ajax({
        type: 'POST',
        url: "<?= get_site_url('',"index.php/wp-json/expo_notification/v2/send"); ?>",
        data: { "id" : id , "title" : title , "key" : "<?= esc_attr( get_option( 'key_expo_notif_device' )) ?>" },
        success: function (result) { alert(JSON.stringify(result)); },
        beforeSend : function () {
            $("#load_"+id).show();
        },
        complete : function () {
            $("#load_"+id).hide();
        }
    });
}					
</script>