<div class="wrap">
	<?php settings_errors(); ?>
    <form method="post" action="options.php">
		<?php 
			settings_fields( 'expo_notification_options_group' );
			    do_settings_sections( 'expo_notification_settings' );
			submit_button();
		?>
    </form>
    
</div>