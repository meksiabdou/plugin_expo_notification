<?php
/**
 * 
 * @package expo_notification
 * 
 */

/*
    Plugin Name: Expo Notification
    Plugin URI: https://meksiabdou.net
    Description: Plugin for push Natification to devices  Android & IOS  
    Version: 1.1
    Author: Meksi Abdennour
    Author URI: https:/fb.com/meksiabdou
    License: GPLv2 or later
    Text Domain: meksiabdou.net
*/


/*

The MIT License (MIT)

Copyright (c) 2019 Meksi Abdennour

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

defined( 'ABSPATH' ) or die( 'Hey, what are you doing here? You silly human!' );

if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

function activatePlugin()
{
    Inc\Base\Activate::activate();
}

register_activation_hook(__FILE__,"activatePlugin");

function DeactivatePlugin()
{
    Inc\Base\Deactivate::deactivate();
}
register_deactivation_hook(__FILE__,"deactivatePlugin");


define( 'PLUGIN_PATH_EXPO', plugin_dir_path( __FILE__ ) );
define( 'PLUGIN_URL_EXPO', plugin_dir_url( __FILE__ ) );

if ( class_exists( 'Inc\\Init' ) ) {
	Inc\Init::register_services();
}


